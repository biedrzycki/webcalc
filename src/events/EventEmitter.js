import Events from 'wolfy87-eventemitter';

const EVENT_UPDATE = 'update';
const EVENT_INITIALIZED = 'initialized';
const EVENT_DELETE = 'delete';

export default class EventEmitter {
  constructor() {
    this._events = null;
    this._initialized = null;
    this.reset();
  }

  reset() {
    this._events = new Events;
    this._initialized = false;
  }

  onUpdate(listener) {
    this._events.on(EVENT_UPDATE, listener);
  }

  onUpdateOnce(listener) {
    this._events.once(EVENT_UPDATE, listener);
  }

  offUpdate(listener) {
    this._events.off(EVENT_UPDATE, listener);
  }

  emitUpdate() {
    this._events.emit(EVENT_UPDATE);
  }

  afterInitialized(listener) {
    if (this._initialized) {
      listener();
    } else {
      this._events.once(EVENT_INITIALIZED, listener);
    }
  }

  emitInitialized() {
    this._initialized = true;
    this._events.emit(EVENT_INITIALIZED);
  }

  onDelete(listener) {
    this._events.on(EVENT_DELETE, listener);
  }

  offDelete(listener) {
    this._events.off(EVENT_DELETE, listener);
  }

  emitDelete(...args) {
    this._events.emit(EVENT_DELETE, ...args);
  }
}
