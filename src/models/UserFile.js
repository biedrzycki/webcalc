import BaseModel from './BaseModel';

export default class UserFile extends BaseModel {
  static type = 'UserFile';

  _content = '';

  constructor(id, content) {
    super(id);

    this.content = content || '';
  }

  toJSON() {
    return {
      id: this.id,
      content: this.content
    }
  }

  get content() {
    return this._content;
  }

  set content(content) {
    this._content = content;

    return this;
  }
}
