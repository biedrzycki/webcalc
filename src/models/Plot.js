export default class Plot {
  constructor(data) {
    this.type = 'Plot';

    this._data = null;
    this.valid = false;
    this.plotData = null;

    this.data = data;
    this._prepareData();
  }

  _prepareData() {
    this.plotData = null;
    this.plotLayout = null;

    const temporaryData = [].concat(this.data);
    let options = this.data.slice(-1)[0];
    const data = [];

    if (options.constructor.name === 'Object') {
      temporaryData.pop();
    } else {
      // empty options if not object
      options = {};
    }

    if (temporaryData.length < 2) {
      return;
    }

    if (temporaryData.length % 2 > 0) {
      return;
    }

    if (temporaryData.some(this._findInvalidMatrix)) {
      return;
    }

    const pairs = this._pairArrays(temporaryData);

    pairs.forEach((pair, index) => {
      const plotDetails = {
        x: [],
        y: [],
        mode: 'lines+markers',
        type: 'scatter'
      };

      if (options.legend && Array.isArray(options.legend._data) && options.legend._data[index]) {
        plotDetails.name = options.legend._data[index];
      }

      pair.forEach((matrix, index) => {
        matrix.forEach((value) => {
          if (index % 2 === 0) {
            plotDetails.x.push(this._getNumberValue(value));
          } else {
            plotDetails.y.push(this._getNumberValue(value));
          }
        });
      });

      data.push(plotDetails);
    });

    this.plotLayout = Object.assign({
      showlegend: true,
      xaxis: {
        title: 'x-axis'
      },
      yaxis: {
        title: 'y-axis'
      }
    }, options);

    this.plotData = [].concat(data);
    this.valid = true;
  }

  _findInvalidMatrix(matrix) {
    return matrix.constructor.name !== 'Matrix' || matrix.size().length > 1;
  }

  _getNumberValue(number) {
    if (number.type === 'BigNumber') {
      return number.toNumber();
    } else if (number.type === 'Complex') {
      return number.re;
    } else if (typeof number === "number") {
      return number;
    } else {
      return 0;
    }
  }

  _pairArrays(a) {
    var temp = a.slice();
    var arr = [];

    while (temp.length) {
      arr.push(temp.splice(0, 2));
    }

    return arr;
  }

  get data() {
    return this._data;
  }

  set data(data) {
    this._data = data;
  }
}
