import BaseModel from './BaseModel';

export default class Document extends BaseModel {
  static type = 'Document';

  _content = '';
  _documentName = '';
  _functionName = '';
  _functionInputParams = '';
  _functionOutputParams = '';

  constructor(id, content, documentName) {
    super(id);

    this.content = content || '';
    this.documentName = documentName || '';
  }

  toJSON() {
    return {
      id: this.id,
      content: this.content,
      documentName: this.documentName,
      functionName: this.functionName,
      functionInputParams: this.functionInputParams,
      functionOutputParams: this.functionOutputParams
    }
  }

  get content() {
    return JSON.parse(this._content);
  }

  set content(content) {
    this._content = JSON.stringify(content);

    return this;
  }

  get documentName() {
    return this._documentName;
  }

  set documentName(name) {
    this._documentName = name;

    return this;
  }

  get functionName() {
    return this._functionName;
  }

  set functionName(name) {
    this._functionName = name;

    return this;
  }

  get functionInputParams() {
    return this._functionInputParams;
  }

  set functionInputParams(params) {
    this._functionInputParams = params;

    return this;
  }

  get functionOutputParams() {
    return this._functionOutputParams;
  }

  set functionOutputParams(params) {
    this._functionOutputParams = params;

    return this;
  }

  get functionInputParamsLength() {
    if (this._functionInputParams.trim().length > 0) {
      return this._functionInputParams.split(',').length;
    }

    return 0;
  }
}
