import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import {hashHistory, Router, Route, IndexRoute} from 'react-router';
import {requireAuth} from './helpers/AuthHelper';

import Main from './components/Main/Main';
import Auth from './components/Auth/Auth';
import Logout from './components/Auth/Logout';
import App from './components/App/App';
import Playground from './components/Playground/Playground';
import Files from './components/Files/Files';
import Editor from './components/Editor/Editor';
import Document from './components/Document/Document';

// CodeMirror at startup
import 'codemirror/addon/edit/closebrackets';
import 'codemirror/addon/edit/matchbrackets';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/ttcn.css';

// Render the main component into the dom
ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={Main}>
      <IndexRoute component={Auth}/>
      <Route component={Logout} path="logout"/>
      <Route component={App} onEnter={requireAuth}>
        <Route path="playground" component={Playground}/>
        <Route path="editor" component={Editor}>
          <Route path="document/:documentId" component={Document}/>
        </Route>
        <Route path="files" component={Files}/>
      </Route>
    </Route>
  </Router>
), document.getElementById('app'));
