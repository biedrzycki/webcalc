'use strict';

// Settings configured here will be merged into the final config object.
export default {
  apiUrl: 'http://api.webcalculus.local/api/v1'
}
