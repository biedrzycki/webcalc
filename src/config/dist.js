'use strict';

import baseConfig from './base';

let config = {
  apiUrl: 'https://api.webcalculus.biedrzycki.info/api/v1'
};

export default Object.freeze(Object.assign({}, baseConfig, config));
