import React, {Component, PropTypes} from 'react';
import evaluateExpression from 'actions/evaluateExpression';
import './Console.scss';

export default class Console extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expression: '',
      working: false
    };

    this.onExpressionChange = (event) => {
      this.setState({expression: event.target.value});
    };

    this.handleKeyUp = (event) => {
      // on enter...
      if (event.keyCode === 13 && this.state.expression.trim().length > 0 && !this.state.working) {
        evaluateExpression(this.state.expression);
        this.setState({expression: ''});
      }
    };

    this.handleLoading = () => {
      this.setState({working: this.props.ResultStore.working});
    }
  }

  componentDidMount() {
    this.props.ResultStore.events.onUpdate(this.handleLoading);
  }

  componentWillUnmount() {
    this.props.ResultStore.events.offUpdate(this.handleLoading);
  }

  render() {
    return (
      <div className="console">
        <input value={this.state.expression}
               autoFocus
               className="console__input"
               type="text"
               name="expression"
               onKeyUp={this.handleKeyUp}
               onChange={this.onExpressionChange}
               placeholder="Type your expression and press Enter..."/>
      </div>
    );
  }
}

Console.propTypes = {
  ResultStore: PropTypes.object.isRequired
};
