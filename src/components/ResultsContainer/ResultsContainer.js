import React, {Component, PropTypes} from 'react';
import ErrorResult from '../Result/ErrorResult';
import BaseResult from '../Result/BaseResult';
import ComplexResult from '../Result/ComplexResult';
import BigNumberResult from '../Result/BigNumberResult';
import MatrixResult from '../Result/MatrixResult';
import PlotResult from '../Result/PlotResult';
import FunctionResult from '../Result/FunctionResult';
import StringResult from '../Result/StringResult';
import BooleanResult from '../Result/BooleanResult';
import LoadingResult from '../Result/LoadingResult';

import {findDOMNode} from 'react-dom';

import './ResultsContainer.scss';

export default class ResultsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: this.props.ResultStore.getRecentResults()
    };

    this.handleNewResult = () => {
      this.setState({results: this.props.ResultStore.getRecentResults()});
    };
  }

  componentDidMount() {
    this.props.ResultStore.events.onUpdate(this.handleNewResult);
    this.scrollContainer();
  }

  componentDidUpdate() {
    this.scrollContainer();
  }

  componentWillUnmount() {
    this.props.ResultStore.events.offUpdate(this.handleNewResult);
  }

  scrollContainer() {
    const container = findDOMNode(this.refs.container);

    container.scrollTop = container.scrollHeight;
  }

  renderResults() {
    return this.state.results.map((resultBody, i) => {
      if (resultBody === null) {
        return false;
      } else if (typeof resultBody.result === 'number') {
        return <BaseResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.type === 'Complex') {
        return <ComplexResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.type === 'BigNumber') {
        return <BigNumberResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.error) {
        return <ErrorResult key={`result-${i}`} error={resultBody}/>;
      } else if (resultBody.type === 'DenseMatrix' || resultBody.type === 'SparseMatrix') {
        return <MatrixResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.type === 'Plot') {
        return <PlotResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.type === 'Loading') {
        return <LoadingResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.type === 'Function') {
        return <FunctionResult key={`result-${i}`} result={resultBody}/>;
      } else if (resultBody.type === 'String') {
        return <StringResult key={`result-${i}`} result={resultBody}/>;
      }  else if (resultBody.type === 'Boolean') {
        return <BooleanResult key={`result-${i}`} result={resultBody}/>;
      }

      return null;
    });
  }

  render() {
    return (
      <div className="results-container" ref="container">
        {this.renderResults()}
      </div>
    );
  }
}

ResultsContainer.propTypes = {
  ResultStore: PropTypes.object.isRequired
};
