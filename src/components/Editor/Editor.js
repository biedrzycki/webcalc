import React, {Component} from 'react';
import Column from '../Column/Column';
import Window from '../Window/Window';
import CodeMirror from './CodeMirror';
import './Editor.scss';
import createDocument from 'actions/createDocument';
import updateDocument from 'actions/updateDocument';
import DocumentStore from 'stores/DocumentStore';
import {Link, withRouter} from 'react-router';

class Editor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editor: null,
      documents: DocumentStore.getDocuments()
    };

    this.documentStoreUpdated = () => {
      this.setState({documents: DocumentStore.getDocuments()});
    }
  }

  componentWillMount() {
    this.onEnter();
  }

  componentDidMount() {
    DocumentStore.events.onUpdate(this.documentStoreUpdated);
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.params.hasOwnProperty('documentId')) {
      this.onEnter();
    }
  }

  componentWillUnmount() {
    DocumentStore.events.offUpdate(this.documentStoreUpdated);
  }

  onEnter() {
    if (this.state.documents.length === 0) {
      createDocument({documentName: 'new_document.m'});
    }

    this.navigateToFirstDocument();
  }

  createDocument() {
    createDocument({documentName: 'new_document.m'});
  }

  navigateToFirstDocument() {
    const document = this.state.documents[0];
    this.props.router.push(`/editor/document/${document.id}`);
  }

  renderDocumentsList() {
    return this.state.documents.map((document) => {
      return (
        <Link to={`/editor/document/${document.id}`}
              className="editor__list__document"
              key={document.id}
              activeClassName="editor__list__document--current">
          {document.documentName}
        </Link>
      )
    });
  }

  handleDocumentSave(instance) {
    const document = DocumentStore.findDocument({id: this.props.params.documentId});

    if (document) {
      updateDocument(document, {content: instance.getValue()});
    }
  }

  render() {
    return (
      <section className="editor">
        <Column width="column-3">
          <Window title="Your scripts">
            <div>
              <div className="editor__new" onClick={this.createDocument.bind(this)}>+ Create new document</div>
              <div className="editor__list">
                {this.renderDocumentsList()}
              </div>
            </div>
          </Window>
        </Column>
        <Column width="column-9">
          <Window title="Editor" titleAlign="left">
            <div className="editor__container">
              <CodeMirror theme="ttcn"
                          onSave={(instance) => this.handleDocumentSave(instance)}
                          onChange={(editor) => this.setState({editor: editor})}/>
              {this.props.children && React.cloneElement(this.props.children, {
                editor: this.state.editor
              })}
            </div>
          </Window>
        </Column>
      </section>
    );
  }
}

export default withRouter(Editor);
