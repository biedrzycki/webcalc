import React, {Component, PropTypes} from 'react';

import CodeMirrorJS from 'codemirror';
import './CodeMirror.scss';

export default class CodeMirror extends Component {
  componentDidMount() {
    this.editor = CodeMirrorJS.fromTextArea(this.refs.editor, {
      gutters: ['note-gutter', 'CodeMirror-linenumbers'],
      mode: 'javascript',
      lineNumbers: true,
      lineWrapping: true,
      smartIndent: false,
      matchBrackets: true,
      autoCloseBrackets: true,
      theme: this.props.theme
    });

    this.editor.setOption('extraKeys', {
      // 'Ctrl-S': this._handleSave.bind(this),
      // 'Cmd-S': this._handleSave.bind(this)
    });

    this.editor.on('change', this._handleChange.bind(this));
    this._handleChange();
  }

  componentWillReceiveProps(nextProps) {
    const contents = nextProps.contents;

    if (this.props.contents !== contents) {
      const oldCursor = this.editor.getCursor();

      this.editor.setValue(contents);
      this.editor.setCursor(oldCursor);
    }
  }

  _handleChange() {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.editor);
    }
  }

  _handleSave(instance) {
    const oldCursor = this.editor.getCursor();

    if (typeof this.props.onSave === 'function') {
      this.props.onSave(instance);
    }

    instance.setCursor(oldCursor);

    return false;
  }

  render() {
    return (
      <div className="code-mirror">
        <textarea ref="editor" defaultValue={this.props.contents}/>
      </div>
    );
  }
}

CodeMirror.propTypes = {
  theme: PropTypes.string.isRequired,
  contents: PropTypes.string,
  onChange: PropTypes.func,
  onSave: PropTypes.func
};
