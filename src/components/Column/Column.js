import React, {Component, PropTypes} from 'react';
import './Column.scss';

export default class Column extends Component {
  render() {
    return (
      <div className={`column ${this.props.width}`}>{this.props.children}</div>
    );
  }
}

Column.propTypes = {
  width: PropTypes.string.isRequired,
  children: PropTypes.object
};
