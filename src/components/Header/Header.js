import React, {Component} from 'react';
import './Header.scss';

export default class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="header__container">
          <h1 className="header__name">WebCalculus</h1>
          <span className="header__version">v1.0.0</span>
        </div>
      </header>
    );
  }
}
