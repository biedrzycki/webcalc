import React, {Component} from 'react';
import AuthForm from './AuthForm';
import loginUserWithJWT from 'actions/Auth/loginUserWithJWT';
import {withRouter} from 'react-router';
import Lockr from 'lockr';
import initializeDocuments from 'actions/initializeDocuments';
import loadFiles from 'actions/UserFile/loadFiles';
import skip from 'actions/Auth/skip';
import './Auth.scss';

class Auth extends Component {
  componentDidMount() {
    const user = Lockr.get('user');

    if (user) {
      loginUserWithJWT(user.token, user.user_id)
        .then((response) => {
          initializeDocuments(response.data.documents);
          loadFiles();

          this.props.router.push('/playground');
        })
        .catch(() => this.props.router.push('/logout'));
    }
  }

  skipLogin() {
    skip().then(() => this.props.router.push('/playground'));
  }

  render() {
    return (
      <div className="auth">
        <div className="auth__wrapper">
          <h1 className="auth__header">WebCalculus</h1>
          <AuthForm/>
          <div className="auth__skip">
            <a onClick={() => this.skipLogin()}>Skip login</a>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Auth);
