import {Component} from 'react';
import {withRouter} from 'react-router';
import logoutUser from 'actions/Auth/logoutUser';

class Logout extends Component {
  componentDidMount() {
    logoutUser();
    this.props.router.push('/');
  }

  render() {
    return null;
  }
}

export default withRouter(Logout);
