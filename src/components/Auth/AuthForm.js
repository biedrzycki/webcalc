import React, {Component} from 'react';
import {Form} from 'formsy-react';
import Input from '../Form/Input';
import loginUser from 'actions/Auth/loginUser';
import initializeDocuments from 'actions/initializeDocuments';
import loadFiles from 'actions/UserFile/loadFiles';
import {withRouter} from 'react-router'

class AuthForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      canSubmit: false
    };
  }

  submit(data) {
    loginUser(data.login, data.password)
      .then((response) => {
        initializeDocuments(response.data.documents);
        loadFiles();

        this.props.router.push('/playground')
      })
      .catch(() => this.props.router.push('/playground'));
  }

  enableButton() {
    this.setState({canSubmit: true});
  }

  disableButton() {
    this.setState({canSubmit: false});
  }

  render() {
    return (
      <Form className="auth__form"
            onValidSubmit={this.submit.bind(this)}
            onValid={this.enableButton.bind(this)}
            onInvalid={this.disableButton.bind(this)}>
        <div className="auth__form__line">
          <label className="auth__form__label">Login:</label>
          <Input name="login" className="auth__form__input" placeholder="Your email" value="biedrzycki.kamil@gmail.com" required={true}/>
        </div>
        <div className="auth__form__line">
          <label className="auth__form__label">Password:</label>
          <Input name="password" className="auth__form__input" placeholder="Your password" value="123qwe" required={true}/>
        </div>
        <div className="auth__form__line">
          <button type="submit" className="auth__form__button" disabled={!this.state.canSubmit}>Sign in</button>
        </div>
      </Form>
    )
  }
}

export default withRouter(AuthForm);
