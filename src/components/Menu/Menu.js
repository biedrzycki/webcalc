import React, {Component} from 'react';
import {Link} from 'react-router';
import './Menu.scss';

export default class Menu extends Component {
  render() {
    return (
      <nav className="menu">
        <Link to="/playground" activeClassName="menu__item--current" className="menu__item">Console</Link>
        <Link to="/editor" activeClassName="menu__item--current" className="menu__item hide-mobile">Editor</Link>
        <Link to="/files" activeClassName="menu__item--current" className="menu__item">Your saved files</Link>
        <Link to="/logout" activeClassName="menu__item--current" className="menu__item menu__item--on-right">Logout</Link>
      </nav>
    );
  }
}
