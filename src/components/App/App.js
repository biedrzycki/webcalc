import React, {Component} from 'react';
import Header from '../Header/Header';
import Menu from '../Menu/Menu';
import './App.scss';

export default class App extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <Menu />
        {this.props.children}
      </div>
    );
  }
}
