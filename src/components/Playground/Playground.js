import React, {Component} from 'react';
import Window from '../Window/Window';
import Console from '../Console/Console';
import Column from '../Column/Column';
import MemoryContainer from '../MemoryContainer/MemoryContainer';
import ResultsContainer from '../ResultsContainer/ResultsContainer';
import ResultStore from 'stores/ResultStore';
import './Playground.scss';

export default class Playground extends Component {
  render() {
    return (
      <section className="playground">
        <Column width="column-3 hide-mobile">
          <Window title="In memory">
            <MemoryContainer />
          </Window>
        </Column>
        <Column width="column-9">
          <Window title="Interactive console">
            <div className="playground__container">
              <ResultsContainer ResultStore={ResultStore} />
              <Console ResultStore={ResultStore} />
            </div>
          </Window>
        </Column>
      </section>
    );
  }
}
