import React, {Component, PropTypes} from 'react';
import './Result.scss';

export default class FunctionResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression}`}
        </div>
      </div>
    );
  }
}

FunctionResult.propTypes = {
  result: PropTypes.object.isRequired
};
