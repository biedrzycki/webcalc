import React, {Component, PropTypes} from 'react';
import './Result.scss';

export default class BooleanResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression.toString()} =`}
          <br />
          <div className="result__separate">
            {`${this.props.result.result.toString()}`}
          </div>
        </div>
      </div>
    );
  }
}

BooleanResult.propTypes = {
  result: PropTypes.object.isRequired
};
