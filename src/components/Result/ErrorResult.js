import React, {Component, PropTypes} from 'react';
import './Result.scss';

export default class ErrorResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__error">
          {`» ${this.props.error.error.message}`}
        </div>
      </div>
    );
  }
}

ErrorResult.propTypes = {
  error: PropTypes.object.isRequired
};
