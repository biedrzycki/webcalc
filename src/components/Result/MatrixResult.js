import React, {Component, PropTypes} from 'react';
import MatrixTable from './MatrixTable/MatrixTable';

export default class MatrixResult extends Component {
  render() {
    const {result} = this.props.result;

    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression} =`}
          <br />
          <div className="result__separate">
            <MatrixTable data={result}/>
          </div>
        </div>
      </div>
    );
  }
}

MatrixResult.propTypes = {
  result: PropTypes.object.isRequired
};
