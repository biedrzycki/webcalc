import React, {Component, PropTypes} from 'react';

export default class BigNumberResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression} =`}
          <br />
          <div className="result__separate">
            {`${this.props.result.result.value}`}
          </div>
        </div>
      </div>
    );
  }
}

BigNumberResult.propTypes = {
  result: PropTypes.object.isRequired
};
