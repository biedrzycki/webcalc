import React, {Component, PropTypes} from 'react';
import Plotly from 'plotly.js/dist/plotly';
import {findDOMNode} from 'react-dom';
import ResultStore from 'stores/ResultStore';
import createFile from 'actions/UserFile/createFile';

export default class PlotResult extends Component {
  constructor(props) {
    super(props);

    this.state = {
      saved: false,
      saving: false
    };
  }

  componentDidMount() {
    this.plotElement = findDOMNode(this.refs.plot);
    const {result: plot} = this.props.result;

    if (!plot.valid) {
      ResultStore.insertResult({
        type: 'error',
        error: Error('Invalid data passed to plot function. Plot takes plot(X:Vector, Y:Vector, [X:Vector, Y:Vector])')
      });

      return;
    }

    this.plot = Plotly.newPlot(this.plotElement, plot.plotData, plot.plotLayout, {
      displaylogo: false
    }).then((gd) => {
      this.gd = gd;
    });
  }

  savePlot() {
    Plotly.toImage(this.gd, {width: 500, height: 500}).then((base64Image) => {
      this.setState({saving: true});

      createFile(base64Image)
        .then(() => this.setState({saving: false, saved: true}));
    });
  }

  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression} =`}
          <br />
          <div className="result__separate">
            <div className="result__plot-container">
              <div className="result__plot" ref="plot"></div>

              {this.state.saved && !this.state.saving && (
                <button className="result__plot__save result__plot__save--saved">Saved!</button>
              )}

              {this.state.saving && !this.state.saved && (
                <button className="result__plot__save result__plot__save--saving">Saving...</button>
              )}

              {!this.state.saved && !this.state.saving && (
                <button className="result__plot__save result__plot__save"
                        onClick={() => this.savePlot()}>
                  Save plot
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Plotly.propTypes = {
  result: PropTypes.object.isRequired
};
