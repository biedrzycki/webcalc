import React, {Component, PropTypes} from 'react';
import './Result.scss';

export default class LoadingResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression} =`}
          <br />
          <div className="result__separate result__separate--loading">
            Calculating...
          </div>
        </div>
      </div>
    );
  }
}

LoadingResult.propTypes = {
  result: PropTypes.object.isRequired
};
