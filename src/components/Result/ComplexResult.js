import React, {Component, PropTypes} from 'react';

export default class ComplexResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression} =`}
          <br />
          <div className="result__separate">
            {`${this.props.result.result.re} + ${this.props.result.result.im}i`}
          </div>
        </div>
      </div>
    );
  }
}

ComplexResult.propTypes = {
  result: PropTypes.object.isRequired
};
