import React, {Component, PropTypes} from 'react';
import './Result.scss';

export default class StringResult extends Component {
  render() {
    return (
      <div className="result">
        <div className="result__base">
          {`» ${this.props.result.expression} =`}
          <br />
          <div className="result__separate">
            {`"${this.props.result.result}"`}
          </div>
        </div>
      </div>
    );
  }
}

StringResult.propTypes = {
  result: PropTypes.object.isRequired
};
