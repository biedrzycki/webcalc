import React, {Component, PropTypes} from 'react';
import MatrixCell from './MatrixCell';

export default class MatrixTableRow extends Component {
  renderCells() {
    const {data} = this.props;

    if (data instanceof Array) {
      return data.map((value, cellIndex) => {
        return <MatrixCell key={`matrix-cell-${cellIndex}`} value={value}/>;
      });
    } else {
      return <MatrixCell value={data} />;
    }
  }

  render() {
    return (
      <tr className="matrix-table__row">
        {this.renderCells()}
      </tr>
    );
  }
}

MatrixTableRow.propTypes = {
  data: PropTypes.any.isRequired
};
