import React, {Component, PropTypes} from 'react';
import mathjs from 'mathjs';

export default class MatrixCell extends Component {
  render() {
    const {value} = this.props;
    let output = value.hasOwnProperty('value') ? value.value : value;

    if (typeof output.mathjs !== 'undefined') {
      if (output.mathjs === 'Complex') {
        output = `${output.re} + ${output.im}i`;
      }
    }

    return (
      <td className="matrix-table__cell">
        {mathjs.format(output, 8).replace(/"/g, '')}
      </td>
    )
  }
}

MatrixCell.propTypes = {
  value: PropTypes.any.isRequired
};
