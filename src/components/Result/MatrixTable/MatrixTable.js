import React, {Component, PropTypes} from 'react';
import MatrixTableRow from './MatrixTableRow';
import './MatrixTable.scss';

export default class MatrixTable extends Component {
  getRows() {
    const size = this.props.data.size;

    if (size.length > 1) {
      return this.props.data.data.map((row, rowIndex) => {
        return <MatrixTableRow key={`matrix-row-${rowIndex}`} data={row}/>;
      });
    } else {
      return <MatrixTableRow data={this.props.data.data}/>;
    }
  }

  render() {
    return (
      <table className="matrix-table">
        <tbody>
          {this.getRows()}
        </tbody>
      </table>
    )
  }
}

MatrixTable.propTypes = {
  data: PropTypes.object.isRequired
};
