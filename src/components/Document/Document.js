import React, {Component, PropTypes} from 'react';
import Runner from '../Runner/Runner';
import './Document.scss';
import DocumentStore from 'stores/DocumentStore';

export default class Document extends Component {
  constructor(props) {
    super(props);

    this.state = {
      document: this.getDocument(props.routeParams.documentId)
    };

    this.documentStoreUpdated = () => {
      this.setState({document: this.getDocument()}, () => {
        this.updateEditor(this.state.document.content);
      });
    }
  }

  componentDidMount() {
    this.state.document && this.updateEditor(this.state.document.content);
    DocumentStore.events.onUpdate(this.documentStoreUpdated);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.routeParams.documentId !== nextProps.routeParams.documentId) {
      this.setState({document: this.getDocument(nextProps.routeParams.documentId)}, () => {
        this.updateEditor(this.state.document.content);
      });
    }

    if (this.props.editor !== nextProps.editor) {
      this.updateEditor(this.state.document.content, nextProps.editor);
    }
  }

  componentWillUnmount() {
    DocumentStore.events.offUpdate(this.documentStoreUpdated);
  }

  getDocument(id) {
    return DocumentStore.findDocument({id: id || this.props.params.documentId})
  }

  updateEditor(content, editor = this.props.editor) {
    editor && editor.setValue(content);
  }

  render() {
    const {document} = this.state;

    if (!document) {
      return null;
    }

    return (
      <div className="document">
        <Runner editor={this.props.editor}
                document={document}/>
      </div>
    )
  }
}

Document.propTypes = {
  editor: PropTypes.object
};
