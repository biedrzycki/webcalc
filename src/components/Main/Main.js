import React, {Component} from 'react';
import './Main.scss';

export default class Main extends Component {
  render() {
    return (
      <div className="main">
        {this.props.children}
      </div>
    )
  }
}
