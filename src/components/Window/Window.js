import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import './Window.scss';

export default class Window extends Component {
  render() {
    const titleClass = classNames({
      window__title: true,
      'window__title--left': this.props.titleAlign === 'left',
      'window__title--right': this.props.titleAlign === 'right'
    });

    return (
      <div className="window">
        <h3 className={titleClass}>{this.props.title}</h3>
        <div className="window__body">
          {this.props.children}
        </div>
      </div>
    );
  }
}

Window.propTypes = {
  title: PropTypes.string.isRequired,
  titleAlign: PropTypes.oneOf(['left', 'center', 'right']),
  children: PropTypes.object
};

Window.defaultProps = {
  titleAlign: 'center'
};
