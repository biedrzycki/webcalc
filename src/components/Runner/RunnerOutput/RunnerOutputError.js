import React, {Component, PropTypes} from 'react';
import {nl2br} from 'helpers/StringHelper';
import './RunnerOutputError.scss';

export default class RunnerOutputError extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="runner-output-error">
        {this.props.error.message}

        <h3 className="runner-output-error__stack-title">Stack trace:</h3>
        <div className="runner-output-error__stack-body"
             dangerouslySetInnerHTML={{__html: nl2br(this.props.error.stack)}}/>
      </div>
    );
  }
}

RunnerOutputError.propTypes = {
  error: PropTypes.object.isRequired
};
