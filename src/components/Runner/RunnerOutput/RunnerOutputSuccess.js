/* eslint no-console: 0 */
import React, {Component, PropTypes} from 'react';
import BaseResult from 'components/Result/BaseResult';
import ComplexResult from 'components/Result/ComplexResult';
import BigNumberResult from 'components/Result/BigNumberResult';
import MatrixResult from 'components/Result/MatrixResult';
import StringResult from 'components/Result/StringResult';
import BooleanResult from 'components/Result/BooleanResult';

import './RunnerOutputSuccess.scss';

export default class RunnerOutputSuccess extends Component {
  constructor(props) {
    super(props);
  }

  prepareParams() {
    const {outputParams} = this.props;

    if (outputParams.trim().length > 0) {
      return outputParams.replace(/\s+/g, '').split(',');
    }

    return [];
  }

  renderResult(variable, output) {
    const resultBody = {
      expression: variable,
      result: output
    };

    if (output === null) {
      return null;
    } else if (typeof output === 'number') {
      return <BaseResult result={resultBody}/>;
    } else if (output.mathjs === 'Complex') {
      return <ComplexResult result={resultBody}/>;
    } else if (output.mathjs === 'BigNumber') {
      return <BigNumberResult result={resultBody}/>;
    } else if (output.mathjs === 'DenseMatrix' || output.mathjs === 'SparseMatrix') {
      return <MatrixResult result={resultBody}/>;
    } else if (output.mathjs === 'String') {
      return <StringResult result={resultBody}/>;
    }  else if (output.mathjs === 'Boolean') {
      return <BooleanResult result={resultBody}/>;
    }
  }

  renderOutput() {
    const params = this.prepareParams();

    return params.map((param, index) => {
      if (this.props.output.scope[param]) {
        return (
          <div className="runner-output-success__param" key={`param-${index}`}>
            {this.renderResult(param, this.props.output.scope[param])}
          </div>
        );
      }

      return (
        <div className="runner-output-success__param" key={`param-${index}`}>
          <div className="runner-output-success__param__name">» {param} = </div>
          <div className="runner-output-success__param__value runner-output-success__param__value--not-found">
            Parameter not found
          </div>
        </div>
      );
    });
  }

  render() {
    console.log(this.props);

    return (
      <div className="runner-output-success">
        <div className="runner-output-success__time">
          Computation time: <b>{this.props.output.time}ms</b>
          {this.renderOutput()}
        </div>
      </div>
    );
  }
}

RunnerOutputSuccess.propTypes = {
  output: PropTypes.object.isRequired,
  outputParams: PropTypes.string,
  running: PropTypes.bool
};
