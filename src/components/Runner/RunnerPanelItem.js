import React, {Component, PropTypes} from 'react';
import './RunnerPanelItem.scss';

export default class RunnerPanelItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="runner-panel-item">
        {this.props.children}
      </div>
    )
  }
}

RunnerPanelItem.propTypes = {
  children: PropTypes.object
};
