import React, {Component, PropTypes} from 'react';
import RunnerOutputError from './RunnerOutput/RunnerOutputError';
import RunnerOutputSuccess from './RunnerOutput/RunnerOutputSuccess';
import './RunnerOutput.scss';

export default class RunnerOutput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {worker} = this.props;

    return (
      <div className="runner-output">
        {worker.latestError && !worker.running && (
          <RunnerOutputError error={worker.latestError}/>
        )}

        {worker.running && (
          <div className="runner-output__running">Running script...</div>
        )}

        {worker.latestResult && !worker.running && (
          <RunnerOutputSuccess outputParams={this.props.outputParams}
                               running={worker.running}
                               output={worker.latestResult}/>
        )}
      </div>
    );
  }
}

RunnerOutput.propTypes = {
  outputParams: PropTypes.string,
  worker: PropTypes.object
};

RunnerOutput.defaultProps = {
  outputParams: ''
};
