import React, {Component, PropTypes} from 'react';
import {Form} from 'formsy-react';
import Input from '../Form/Input';
import Document from 'models/Document';
import classNames from 'classnames';
import './RunnerProperties.scss';

export default class RunnerProperties extends Component {
  constructor(props) {
    super(props);

    this.state = {
      canSubmit: false,
      isPristine: true
    }
  }

  submit(data) {
    if (typeof this.props.onSave === 'function') {
      this.props.onSave(data);
    }
  }

  enableButton() {
    this.setState({canSubmit: true});
  }

  disableButton() {
    this.setState({canSubmit: false});
  }

  checkPristine(model, changed) {
    if (typeof changed === 'undefined') {
      changed = this.refs.form.isChanged();
    }

    if (this.props.editor.getValue() !== this.props.document.content) {
      return;
    }

    this.setState({isPristine: !changed});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.editor) {
      this.setState({isPristine: nextProps.editor.getValue() === nextProps.document.content});
    }
  }

  render() {
    const {document} = this.props;
    const buttonClass = classNames({
      'runner-properties__button': true,
      'runner-properties__button--changed': !this.state.isPristine
    });

    return (
      <Form className="runner-properties"
            ref="form"
            onChange={this.checkPristine.bind(this)}
            onValidSubmit={this.submit.bind(this)}
            onValid={this.enableButton.bind(this)}
            onInvalid={this.disableButton.bind(this)}>
        <div className="runner-properties__line">
          <label htmlFor="documentName" className="runner-properties__label">
            Document title:
          </label>
          <Input value={document.documentName}
                 name="documentName"
                 className="runner-properties__input"
                 required={true}/>
        </div>
        <div className="runner-properties__line">
          <label htmlFor="functionName" className="runner-properties__label">
            Function name:
          </label>
          <Input value={document.functionName}
                 name="functionName"
                 className="runner-properties__input"/>
        </div>
        <div className="runner-properties__line">
          <label htmlFor="functionInputParams" className="runner-properties__label">
            Function input parameters:
            <div className="runner-properties__label__tip">
              provide few by separating with commas, you can provide default values for variables, f.e. n = 100
            </div>
          </label>
          <Input value={document.functionInputParams}
                 name="functionInputParams"
                 className="runner-properties__input"/>
        </div>
        <div className="runner-properties__line">
          <label htmlFor="functionOutputParams" className="runner-properties__label">
            Function output parameters:
            <div className="runner-properties__label__tip">
              provide few by separating with commas
            </div>
          </label>
          <Input value={document.functionOutputParams}
                 name="functionOutputParams"
                 className="runner-properties__input"/>
        </div>
        <div className="runner-properties__line" style={{textAlign: 'right', marginTop: '15px'}}>
          <button type="submit"
                  className={buttonClass}
                  disabled={!this.state.canSubmit}>
            Save document
          </button>
        </div>
      </Form>
    );
  }
}

RunnerProperties.propTypes = {
  onSave: PropTypes.func,
  document: PropTypes.instanceOf(Document).isRequired,
  editor: PropTypes.object
};
