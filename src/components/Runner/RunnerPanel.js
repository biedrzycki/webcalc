import React, {Component, PropTypes} from 'react';
import RunnerPanelItem from './RunnerPanelItem';
import {MdStop, MdPlayArrow, MdDelete} from 'react-icons/md';

export default class RunnerPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="runner-panel">
        <RunnerPanelItem>
          <MdPlayArrow size={26} color={'green'} onClick={this.props.onPlay.bind(this)}/>
        </RunnerPanelItem>
        <RunnerPanelItem>
          <MdStop size={26} color={'red'} onClick={this.props.onStop.bind(this)}/>
        </RunnerPanelItem>
        <RunnerPanelItem>
          <MdDelete size={20} onClick={this.props.onDelete.bind(this)}/>
        </RunnerPanelItem>
      </div>
    );
  }
}

RunnerPanel.propTypes = {
  onPlay: PropTypes.func.isRequired,
  onStop: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
};
