import React, {Component, PropTypes} from 'react';
import './Runner.scss';
import RunnerOutput from './RunnerOutput';
import RunnerPanel from './RunnerPanel';
import RunnerProperties from './RunnerProperties';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import updateDocument from 'actions/updateDocument';
import removeDocument from 'actions/removeDocument';
import WorkerStore from 'stores/WorkerStore';
import Document from 'models/Document';
import {withRouter} from 'react-router';
import startWorker from 'actions/Worker/startWorker';

class Runner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      worker: null
    };

    this.onWorkerChange = () => {
      this.setState({worker: WorkerStore.getWorker(this.props.document.id)});
    }
  }

  componentDidMount() {
    this.setState({worker: WorkerStore.getWorker(this.props.document.id)});
    WorkerStore.events.onUpdate(this.onWorkerChange);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({worker: WorkerStore.getWorker(nextProps.document.id)});
  }

  componentWillUnmount() {
    WorkerStore.events.offUpdate(this.onWorkerChange);
  }

  runWorker() {
    startWorker(this.props.document.id, {
      type: 'editor',
      script: this.props.editor.getValue(),
      inputParams: this.props.document.functionInputParams
    });
  }

  stopWorker() {
    WorkerStore.stopWorker(this.props.document.id);
  }

  saveDocument(properties) {
    this.stopWorker();
    updateDocument(this.props.document, Object.assign(properties, {
      content: this.props.editor.getValue()
    }));
  }

  removeDocument() {
    if (confirm('Are you sure?')) {
      this.props.router.push('/editor');
      removeDocument(this.props.document);
    }
  }

  render() {
    return (
      <div className="runner">
        <div className="runner__panel">
          <RunnerPanel onPlay={this.runWorker.bind(this)}
                       onDelete={this.removeDocument.bind(this)}
                       onStop={this.stopWorker.bind(this)}/>
        </div>
        <div className="runner__content">
          <Tabs>
            <TabList>
              <Tab>Document</Tab>
              <Tab>Output</Tab>
            </TabList>

            <TabPanel>
              <RunnerProperties document={this.props.document}
                                editor={this.props.editor}
                                onSave={(properties) => this.saveDocument(properties)}/>
            </TabPanel>
            <TabPanel>
              <h2 className="runner__content__title">Console output</h2>
              <RunnerOutput outputParams={this.props.document.functionOutputParams}
                            worker={this.state.worker || null}/>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    );
  }
}

Runner.propTypes = {
  editor: PropTypes.object,
  document: PropTypes.instanceOf(Document).isRequired
};

export default withRouter(Runner);
