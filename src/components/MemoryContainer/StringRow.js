import React, {Component, PropTypes} from 'react';

export default class StringRow extends Component {
  render() {
    return (
      <div className="memory-container__val">
        <span>
          "{this.props.value}"
        </span>
      </div>
    );
  }
}

StringRow.propTypes = {
  value: PropTypes.any.isRequired
};
