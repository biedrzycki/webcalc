import React, {Component} from 'react';
import MemoryContainerRow from './MemoryContainerRow';
import ScopeStore from '../../stores/ScopeStore';
import './MemoryContainer.scss';

export default class MemoryContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scope: ScopeStore.getScope() || {}
    };

    this.handleScopeUpdate = () => {
      this.setState({scope: ScopeStore.getScope()});
    };
  }

  componentDidMount() {
    ScopeStore.events.onUpdate(this.handleScopeUpdate);
  }

  componentWillUnmount() {
    ScopeStore.events.offUpdate(this.handleScopeUpdate);
  }

  getScope() {
    return Object.keys(this.state.scope).map((scopeVariable, index) => {
      return <MemoryContainerRow key={`memory-${index}`}
                                 value={this.state.scope[scopeVariable]}
                                 variable={scopeVariable}/>;
    });
  }

  render() {
    return (
      <div className="memory-container">
        <div className="memory-container__table">
          {this.getScope()}
        </div>
      </div>
    );
  }
}
