import React, {Component, PropTypes} from 'react';
import ResultStore from 'stores/ResultStore';

export default class FunctionRow extends Component {
  render() {
    const func = ResultStore.findResult({
      type: 'Function',
      variable: this.props.variable
    });

    return (
      <div className="memory-container__val">
        <span>
          {func.expression.split('=')[1].trim()}
        </span>
      </div>
    );
  }
}

FunctionRow.propTypes = {
  value: PropTypes.any.isRequired
};
