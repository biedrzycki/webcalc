import React, {Component, PropTypes} from 'react';

export default class BigNumberRow extends Component {
  render() {
    return (
      <div className="memory-container__val">
        <span>
          {this.props.value.value.toString()}
        </span>
      </div>
    );
  }
}

BigNumberRow.propTypes = {
  value: PropTypes.any.isRequired
};
