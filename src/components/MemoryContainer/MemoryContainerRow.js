import React, {Component, PropTypes} from 'react';
import BaseMemoryRow from './BaseMemoryRow';
import ComplexMemoryRow from './ComplexMemoryRow';
import BigNumberRow from './BigNumberRow';
import FunctionRow from './FunctionRow';
import MatrixRow from './MatixRow';
import StringRow from './StringRow';
import BooleanRow from './BooleanRow';

import evaluateExpression from '../../actions/evaluateExpression';

export default class MemoryContainerRow extends Component {
  getContainerRow() {
    if (typeof this.props.value === 'number') {
      return <BaseMemoryRow {...this.props} />;
    } else if (this.props.value.mathjs === 'Complex') {
      return <ComplexMemoryRow {...this.props} />;
    } else if (this.props.value.mathjs === 'BigNumber') {
      return <BigNumberRow {...this.props} />;
    } else if (typeof this.props.value === 'function') {
      return <FunctionRow {...this.props} value="function" />;
    } else if (this.props.value.mathjs === 'DenseMatrix' || this.props.value.mathjs === 'SparseMatrix') {
      return <MatrixRow {...this.props} />;
    } else if (typeof this.props.value === 'string') {
      return <StringRow {...this.props} />;
    } else if (typeof this.props.value === 'boolean') {
      return <BooleanRow {...this.props} />;
    }
  }

  retrieveFromHistory(expression) {
    return evaluateExpression(expression);
  }

  render() {
    return (
      <div title={this.props.value.toString()}
           onClick={() => { this.retrieveFromHistory(this.props.variable) }}
           className="memory-container__row">
        <div className="memory-container__var">
          <strong>{this.props.variable}</strong>
        </div>
        {this.getContainerRow()}
      </div>
    );
  }
}

MemoryContainerRow.propTypes = {
  variable: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired
};
