import React, {Component, PropTypes} from 'react';

export default class BooleanRow extends Component {
  render() {
    return (
      <div className="memory-container__val">
        <span>
          {this.props.value.toString()}
        </span>
      </div>
    );
  }
}

BooleanRow.propTypes = {
  value: PropTypes.any.isRequired
};
