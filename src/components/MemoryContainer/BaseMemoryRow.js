import React, {Component, PropTypes} from 'react';

export default class BaseMemoryRow extends Component {
  render() {
    return (
      <div className="memory-container__val">{this.props.value}</div>
    );
  }
}

BaseMemoryRow.propTypes = {
  value: PropTypes.any.isRequired
};
