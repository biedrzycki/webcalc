import React, {Component, PropTypes} from 'react';

export default class MatrixRow extends Component {
  getMatrixSize() {
    const sizeArray = this.props.value.size;

    if (sizeArray.length > 1) {
      return sizeArray.join('x');
    }

    return `1x${sizeArray}`
  }

  render() {
    return (
      <div className="memory-container__val">
        <span>
          Matrix [{this.getMatrixSize()}]
        </span>
      </div>
    );
  }
}

MatrixRow.propTypes = {
  value: PropTypes.any.isRequired
};
