import React, {Component, PropTypes} from 'react';

export default class ComplexMemoryRow extends Component {
  render() {
    return (
      <div className="memory-container__val">{this.props.value.re} + {this.props.value.im}i</div>
    );
  }
}

ComplexMemoryRow.propTypes = {
  value: PropTypes.any.isRequired
};
