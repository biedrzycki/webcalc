import React, {Component} from 'react';
import Window from '../Window/Window';
import Column from '../Column/Column';
import './Files.scss';
import FileStore from 'stores/FileStore';
import File from './File';

export default class Files extends Component {
  constructor(props) {
    super(props);

    this.state = {
      files: []
    };

    this.handleFilesChange = () => {
      this.setState({files: FileStore.getFiles()});
    }
  }

  componentWillMount() {
    FileStore.events.onUpdate(this.handleFilesChange);
  }

  componentDidMount() {
    this.handleFilesChange();
  }

  componentWillUnmount() {
    FileStore.events.offUpdate(this.handleFilesChange);
  }

  render() {
    return (
      <section className="files">
        <Column width="column-12">
          <Window title="Your files">
            <div className="files__container">
              {this.state.files.map(file => {
                return <File key={file.id} file={file}/>;
              })}
            </div>
          </Window>
        </Column>
      </section>
    );
  }
}
