import React, {Component} from 'react';
import removeFile from 'actions/UserFile/removeFile';
import {MdDelete} from 'react-icons/md';
import './File.scss';

export default class File extends Component {
  openFile() {
    window.open(this.props.file.content, '_blank');
  }

  handleRemove() {
    if (confirm('Are you sure?')) {
      removeFile(this.props.file);
    }
  }

  render() {
    return (
      <div className="file">
        <img onClick={() => this.openFile()} className="file__thumb" src={this.props.file.content} alt="" />
        <span className="file__remove">
          <MdDelete size={20} onClick={() => this.handleRemove()}/>
        </span>
      </div>
    );
  }
}
