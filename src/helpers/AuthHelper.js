import LoginStore from 'stores/LoginStore';

export function requireAuth(nextState, replace) {
  if (!LoginStore.isUserLogged() && !LoginStore.skip) {
    replace({pathname: '/', state: {nextPathname: nextState.location.pathname}});
  }
}
