module.exports.buildScript = function(content, inputParams = '', inputParamsValues = []) {
  let modifiedContent = '(function() { ';
  modifiedContent += 'console.time(\'editor\'); ';

  const params = prepareParams(inputParams);

  params.forEach((parameter, index) => {
    if (parameter.length < 1) {
      return;
    }

    const variable = getVariable(parameter);
    modifiedContent += `var ${variable} = `;

    if (parameter.indexOf('=') > -1 && typeof inputParamsValues[index] === 'undefined') {
      modifiedContent += `mathParser.eval('${parameter}');\n`;
    } else {
      let defaultValue = null;

      if (typeof inputParamsValues[index] !== 'undefined') {
        defaultValue = inputParamsValues[index];
      }

      modifiedContent += `mathParser.eval('${variable} = ${defaultValue}');\n`;
    }
  });

  content.split('\n').forEach((line) => {
    const trimmedLine = line.trim();

    if (trimmedLine.length < 1) {
      return;
    }

    // this will detect statements with curly braces - dummy check
    if (line.match(/[{}#]/) || line.match(/\+{2}/) || line.match(/\-{2}/)) {
      if (line.match(/for/)) {
        const statementVariable = getStatementVariable(trimmedLine);
        modifiedContent += `${getLoopStatement(trimmedLine)}\n`;
        modifiedContent += `mathParser.eval('${statementVariable} = ' + ${statementVariable});\n`;
      } else if (line.match(/if/) || line.match(/else/) || line.match(/while/)) {
        modifiedContent += `${trimmedLine}\n`;
      } else if (line.match(/[}]/)) {
        modifiedContent += `${trimmedLine}\n`;
      } else if (line.match(/\+{2}/) || line.match(/\-{2}/)) {
        const statementVariable = getQuickVariable(trimmedLine);
        modifiedContent += `${trimmedLine}\n`;
        modifiedContent += `mathParser.eval('${statementVariable} = ' + ${statementVariable});\n`;
      }
    } else {
      const statementVariable = getStatementVariable(trimmedLine);
      const arrayVariable = getArrayVariable(trimmedLine);

      if (arrayVariable) {
        modifiedContent += `${arrayVariable} = `;
      } else {
        modifiedContent += `var ${statementVariable} = `;
      }

      modifiedContent += `mathParser.eval('${trimmedLine}');\n`;
    }
  });

  modifiedContent += ' console.timeEnd(\'editor\');';
  modifiedContent += ' })();';

  return modifiedContent;
};

function getLoopStatement(statementLine) {
  const pattern = /for\s*\(\s*/;

  return statementLine.replace(pattern, 'for (var ');
}

function getStatementVariable(statementLine) {
  const pattern = /\s*[\_a-zA-Z0-9]*\s*\=/;
  const match = statementLine.match(pattern);

  if (match) {
    return match[0].replace('/\s+/g', '').replace('=', '').trim();
  }
}

function getArrayVariable(statementLine) {
  const pattern = /\s*[\_a-zA-Z0-9]*\[[\_a-zA-Z0-9\,\s]*\]\s*\=/;
  const match = statementLine.match(pattern);

  if (match) {
    return match[0].replace('/\s+/g', '').replace('=', '').trim();
  }
}

function getQuickVariable(statementLine) {
  return statementLine.replace('++', '').replace('--', '');
}

function getVariable(statement) {
  return statement.split('=')[0].trim();
}

function prepareParams(params) {
  if (params.trim().length > 0) {
    return params.replace(/\s+/g, '').split(',');
  }

  return [];
}
