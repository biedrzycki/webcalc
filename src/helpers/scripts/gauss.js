export default (n, max) => {
  const A = randomArray(n, n, max);
  const b = randomArray(1, n, max)[1];

  console.time('gauss');
  const result = gauss(A, b);

  console.timeEnd('gauss');

  return result;
}

function randomArray(n, m, max) {
  const A = [];

  for (let k = 1; k <= n; k++) {
    A[k] = [];

    for (let z = 1; z <= m; z++) {
      A[k][z] = Math.ceil(Math.random() * max);
    }
  }

  return A;
}

function gauss(a, b) {
  const n = b.length - 1;
  const x = randomArray(1, n, 0)[1];
  const A = a;

  for (let i = 1; i < A.length; i++) {
    A[i].push(b[i]);
  }

  for (let j = 1; j <= n - 1; j++) {
    for (let i = j + 1; i <= n; i++) {
      const coef = A[i][j] / A[j][j];

      for (let k = 1; k < A[i].length; k++) {
        A[i][k] = A[i][k] - A[j][k] * coef;
      }
    }
  }

  let sum = 0;

  for (let i = n; i >= 1; i--) {
    for (let j = i; j <= n - 1; j++) {
      sum = sum + A[i][j+1] * x[j+1];
      x[i] = (A[i][n+1] - sum) / A[i][i];
    }
  }

  return x;
}
