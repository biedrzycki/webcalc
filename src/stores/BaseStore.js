import EventEmitter from '../events/EventEmitter';

export default class BaseStore {
  constructor() {
    this._initialized = null;
    this._events = new EventEmitter();
  }

  isInitialized() {
    return this._initialized;
  }

  init() {
    this._initialized = true;
    this.events.emitInitialized();
  }

  resetInit() {
    this._initialized = false;
    this.events.reset();
  }

  /**
   * @returns {EventEmitter}
   */
  get events() {
    return this._events;
  }
}
