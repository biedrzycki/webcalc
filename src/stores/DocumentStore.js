import BaseStore from './BaseStore';
import _ from 'lodash';
import Document from 'models/Document';
import ParserService from 'services/ParserService';

class DocumentStore extends BaseStore {
  constructor() {
    super();

    /**
     * @type {Document[]}
     * @private
     */
    this._documents = [];
  }

  /**
   * @param documents {Document[]}
   */
  init(documents) {
    this._documents = documents;
    super.init();
  }

  resetInit() {
    this._documents = [];
    super.resetInit();
  }

  /**
   * Document to be added
   * @param document {Document}
   */
  addDocument(document) {
    this._documents.push(document);
    this.events.emitUpdate();
  }

  /**
   * Document to be removed
   * @param document {Document}
   */
  removeDocument(document) {
    _.remove(this._documents, document);
    this.events.emitUpdate();
  }

  findDocument(params) {
    return _.find(this._documents, params);
  }

  findDocumentIndex(params) {
    return _.findIndex(this._documents, params);
  }

  updateDocument(document, params) {
    const documentIndex = this.findDocumentIndex({id: document.id});

    if (documentIndex > -1) {
      this._documents.splice(documentIndex, 1, Document.create(Object.assign({}, document.toJSON(), params)));
      this.updateFunctions(document.id);
      this.events.emitUpdate();
    }
  }

  updateFunctions(documentId) {
    const document = this.findDocument({id: documentId});

    if (document.functionName && document.functionName.length > 0) {
      ParserService.addFunction(document.functionName, document);
    }
  }

  /**
   * Return available documents from store
   * @returns {Document[]}
   */
  getDocuments() {
    return this._documents;
  }
}

export default new DocumentStore();
