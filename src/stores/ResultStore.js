import BaseStore from './BaseStore';
import ParserService from '../services/ParserService';
import ScopeStore from './ScopeStore';
import _ from 'lodash';

class ResultStore extends BaseStore {
  constructor() {
    super();

    this._results = [];
    this.working = false;

    ParserService.addPreservedWord('clear', () => {
      ParserService.clear().then(() => ScopeStore.clear());

      this._results = [];

      this.events.emitUpdate();

      return true;
    });
  }

  init(results) {
    this._results = results;
    super.init();
  }

  resetInit() {
    this._results = [];
    super.resetInit();
  }

  insertResult(result) {
    this._results.push(result);
    this.events.emitUpdate();
  }

  getAllResults() {
    return this._results;
  }

  getRecentResults(limit = 30) {
    return this._results.slice(-limit);
  }

  getLastResult() {
    return this.getRecentResults(1)[0];
  }

  findResult(params) {
    return _.find(this._results, params);
  }

  removeLastResult() {
    this._results.pop();
    this.events.emitUpdate();
  }

  setWorking() {
    this.working = true;
  }

  setNotWorking() {
    this.working = false;
  }
}

export default new ResultStore();
