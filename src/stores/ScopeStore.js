import BaseStore from './BaseStore';

class ScopeStore extends BaseStore {
  constructor() {
    super();

    this._scope = {};
  }

  update(scope) {
    this._scope = Object.assign({}, this._scope, scope);
    this.events.emitUpdate();
  }

  clear() {
    this._scope = {};
    this.events.emitUpdate();
  }

  getScopeVariable(variable) {
    return this._scope[variable];
  }

  getScope() {
    return this._scope;
  }
}

export default new ScopeStore();
