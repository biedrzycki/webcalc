import BaseStore from './BaseStore';
import Lockr from 'lockr';

class LoginStore extends BaseStore {
  constructor() {
    super();
    this._user_id = null;
    this._jwt = null;
    this._skip = false;
  }

  loginUser(jwt, user_id) {
    this._jwt = jwt;
    this._user_id = user_id;
    Lockr.set('user', {token: this._jwt, user_id: this._user_id});

    this.events.emitUpdate();
    this.events.emitInitialized();
  }

  logoutUser() {
    this._jwt = null;
    this._user_id = null;
    Lockr.rm('user');

    this.events.emitUpdate();
  }

  get user() {
    return this._user_id;
  }

  get jwt() {
    return this._jwt;
  }

  get skip() {
    return this._skip;
  }

  isUserLogged() {
    return !!this._user_id;
  }
}

export default new LoginStore();
