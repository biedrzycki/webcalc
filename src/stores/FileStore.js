import BaseStore from './BaseStore';
import _ from 'lodash';

class FileStore extends BaseStore {
  constructor() {
    super();

    /**
     * @type {UserFile[]}
     * @private
     */
    this._files = [];
  }

  /**
   * @param files {UserFile[]}
   */
  init(files) {
    this._files = files;
    super.init();
  }

  resetInit() {
    this._files = [];
    super.resetInit();
  }

  /**
   * File to be added
   * @param file {UserFile}
   */
  addFile(file) {
    this._files.push(file);
    this.events.emitUpdate();
  }

  /**
   * Document to be removed
   * @param file {UserFile}
   */
  removeFile(file) {
    _.remove(this._files, file);
    this.events.emitUpdate();
  }

  findFile(params) {
    return _.find(this._files, params);
  }

  findFileIndex(params) {
    return _.findIndex(this._files, params);
  }

  /**
   * Return available files from store
   * @returns {UserFile[]}
   */
  getFiles() {
    return this._files;
  }
}

export default new FileStore();
