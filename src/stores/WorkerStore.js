import BaseStore from './BaseStore';
import PromiseWorker from 'promise-worker';

// worker
const MathWorker = require('worker!./../services/MathWorker');

class WorkerStore extends BaseStore {
  constructor() {
    super();

    this._workers = {};
  }

  clear() {
    this._workers = {};
    this.events.emitUpdate();
  }

  createWorker(id, additionalParams = {}) {
    const worker = new MathWorker;
    const PromisifiedMathWorker = new PromiseWorker(worker);

    this._workers[id] = Object.assign({
      running: false,
      worker: worker,
      latestResult: null,
      latestError: null,
      promisifiedWorker: PromisifiedMathWorker
    }, additionalParams);

    this.events.emitUpdate();
  }

  stopWorker(id) {
    if (this._workers[id]) {
      this._workers[id].worker.terminate();
      this.createWorker(id);

      this.events.emitUpdate();
    }
  }

  setRunning(id) {
    if (this._workers[id]) {
      this._workers[id].running = true;

      this.events.emitUpdate();
    }
  }

  setResult(id, result) {
    if (this._workers[id]) {
      this._workers[id].latestResult = result;
      this._workers[id].latestError = null;
      this._workers[id].running = false;

      this.events.emitUpdate();
    }
  }

  setError(id, error) {
    if (this._workers[id]) {
      this._workers[id].latestResult = null;
      this._workers[id].latestError = error;
      this._workers[id].running = false;

      this.events.emitUpdate();
    }
  }

  removeWorker(id) {
    if (this._workers[id]) {
      this._workers[id].worker.terminate();
      delete this._workers[id];

      this.events.emitUpdate();
    }
  }

  getWorker(id) {
    return this._workers[id];
  }

  getWorkers() {
    return this._workers;
  }
}

export default new WorkerStore();
