import AuthService from 'services/AuthService';
import LoginStore from 'stores/LoginStore';

export default function loginUserWithJWT(token, userId) {
  const authPromise = AuthService.check(token, userId);

  authPromise.then((response) => {
    LoginStore.loginUser(response.data.token, response.data.id);
  }).catch(() => {
    LoginStore.logoutUser();
  });

  return authPromise;
}
