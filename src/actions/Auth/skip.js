import LoginStore from 'stores/LoginStore';

export default function skip() {
  return new Promise((resolve) => {
    LoginStore._skip = true;

    setTimeout(() => resolve());
  });
}
