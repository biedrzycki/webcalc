import AuthService from 'services/AuthService';
import LoginStore from 'stores/LoginStore';

export default function loginUser(login, password) {
  const authPromise = AuthService.login(login, password);

  authPromise.then((response) => {
    LoginStore.loginUser(response.data.token, response.data.id);
  }).catch(() => {
    LoginStore.logoutUser();
  });

  return authPromise;
}
