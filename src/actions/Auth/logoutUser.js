import AuthService from 'services/AuthService';
import LoginStore from 'stores/LoginStore';

export default function logoutUser() {
  AuthService.logout();
  LoginStore.logoutUser();
}
