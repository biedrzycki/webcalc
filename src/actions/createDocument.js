import DocumentStore from 'stores/DocumentStore';
import WorkerStore from 'stores/WorkerStore';
import Document from 'models/Document';

export default function createDocument(documentParams) {
  const document = Document.create(documentParams);
  DocumentStore.addDocument(document);
  WorkerStore.createWorker(document.id);

  DocumentStore.updateFunctions(document.id);
}
