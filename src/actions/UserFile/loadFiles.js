import FileService from 'services/FileService';
import FileStore from 'stores/FileStore';
import UserFile from 'models/UserFile';

export default function loadFiles() {
  const filesPromise = FileService.load();

  filesPromise.then(response => {
    const files = response.data.map((fileParams) => {
      return UserFile.create({
        id: fileParams.id,
        content: fileParams.file_content
      });
    });

    FileStore.init(files);
  });

  return filesPromise;
}
