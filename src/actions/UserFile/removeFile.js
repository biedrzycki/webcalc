import FileStore from 'stores/FileStore';
import FileService from 'services/FileService';

export default function removeFile(file) {
  FileStore.removeFile(file);

  return FileService.remove(file.id);
}
