import FileStore from 'stores/FileStore';
import FileService from 'services/FileService';
import UserFile from 'models/UserFile';

export default function createFile(content) {
  const file = UserFile.create({content});
  FileStore.addFile(file);

  return FileService.add(file.id, file.content);
}
