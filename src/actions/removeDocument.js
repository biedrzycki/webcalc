import DocumentStore from 'stores/DocumentStore';
import ParserService from 'services/ParserService';
import WorkerStore from 'stores/WorkerStore';
import DocumentService from 'services/DocumentService';

export default function removeDocument(document) {
  DocumentStore.removeDocument(document);
  ParserService.removeFunction(document.functionName);
  WorkerStore.removeWorker(document.id);

  return DocumentService.remove(document.id);
}
