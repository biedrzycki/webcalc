import WorkerStore from 'stores/WorkerStore';

export default function startWorker(id, msg) {
  const worker = WorkerStore.getWorker(id);
  const promise = worker.promisifiedWorker.postMessage(msg);

  WorkerStore.setRunning(id);
  promise
    .then((result) => WorkerStore.setResult(id, result))
    .catch((error) => WorkerStore.setError(id, error));

  return promise;
}
