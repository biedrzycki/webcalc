/* eslint no-console: 0 */
import ResultStore from 'stores/ResultStore';
import ScopeStore from 'stores/ScopeStore';
import ParserService from '../services/ParserService';

export default function evaluateExpression(expression) {
  let result;
  let skip = false;

  const variable = expression.split('=')[0].trim();

  // check preserved words first
  const preserved = ParserService.check(variable);

  if (preserved) {
    result = {
      type: preserved.type,
      expression: `${variable} `,
      result: preserved.result || preserved
    };

    skip = true;
  }

  // check scope availability
  if (expression.indexOf('=') === -1) {
    const inScope = ScopeStore.getScopeVariable(variable);

    if (inScope) {
      result = parseResponse({output: inScope}, expression);

      skip = true;
    }
  }

  // check preserved (custom) function
  const preservedFunction = ParserService.checkFunction(expression);

  if (preservedFunction) {
    insertLoadingResult(variable);
    ResultStore.setWorking();

    result = produceFunctionResult(expression, preservedFunction);
  }

  if (result instanceof Promise) {
    result.then((response) => {
      const outputVariables = response.document.functionOutputParams.replace(/\s*/g, '').split(',');
      const trimmedExpression = expression.trim();
      let functionCall = expression.trim();
      let variablesToAssign = [];

      if (expression.indexOf('=') > -1) {
        functionCall = expression.trim();
        variablesToAssign = expression.split('=')[0].trim().replace(/[\[\]\(\)\s*]/g, '').split(',');
      }

      ResultStore.setNotWorking();
      ResultStore.removeLastResult();

      outputVariables.forEach((variable, index) => {
        if (trimmedExpression[trimmedExpression.length - 1] !== ';') {
          const output = parseResponse({output: response.scope[variable]}, `${functionCall} -> ${variable}`);
          ResultStore.insertResult(output);
        }

        if (typeof variablesToAssign[index] !== 'undefined') {
          ScopeStore.update({[variablesToAssign[index]]: response.scope[variable]});
        }
      });

      // update console worker scope
      ParserService.updateScope(ScopeStore.getScope());
    });

    return;
  } else if (result && result.type === 'error') {
    skip = true;
  }

  // try to parse then...

  if (!skip) {
    ResultStore.setWorking();
    insertLoadingResult(variable);

    ParserService.eval(expression).then((response) => {
      result = parseResponse(response, expression);

      ResultStore.setNotWorking();
      ResultStore.removeLastResult();
      ResultStore.insertResult(result);

      if (response.scope) {
        ScopeStore.update(response.scope);
      }
    }).catch((error) => {
      result = {
        type: 'error',
        error: error
      };

      console.log(error);

      ResultStore.setNotWorking();
      ResultStore.insertResult(result);
    });
  } else {
    ResultStore.setNotWorking();
    ResultStore.insertResult(result);

    return result;
  }
}

function parseResponse(response, expression) {
  const type = response.output.mathjs || response.output.type;
  const evaluation = response.output;

  let result;

  if (type === 'ResultSet') {
    result = null;
  } else if (type === 'SparseMatrix' || type === 'DenseMatrix') {
    result = {
      type: type,
      expression: expression,
      result: evaluation
    }
  } else if (typeof evaluation.value === 'function') {
    result = {
      type: 'Function',
      expression: expression,
      result: evaluation,
      variable: evaluation.name
    }
  } else if (type === 'BigNumber') {
    result = {
      type: type,
      expression: expression,
      result: evaluation
    };
  } else if (typeof evaluation === 'string') {
    result = {
      type: 'String',
      expression: expression,
      result: evaluation
    }
  } else if (typeof evaluation === 'boolean') {
    result = {
      type: 'Boolean',
      expression: expression,
      result: evaluation
    }
  } else if (typeof evaluation === 'number') {
    result = {
      type: 'Number',
      expression: expression,
      result: evaluation
    };
  } else {
    result = {
      type: type || 'String',
      expression: expression,
      result: evaluation
    };
  }

  return result;
}

function produceFunctionResult(expression, preservedFunction) {
  const functionParamsMatch = /\(\s*([^)]+?)\s*\)/.exec(expression);

  let functionParams = [];

  if (functionParamsMatch) {
    functionParams = functionParamsMatch[1].split(/\s*,\s*/);
  }

  const call = preservedFunction(...functionParams);

  if (call.error) {
    return {
      type: 'error',
      error: new Error(`Wrong number of parameters passed - ${functionParams.length} passed, ${call.paramsAmount} required.`)
    }
  }

  return call.body.then((response) => {
    return {
      scope: response.scope,
      document: call.document
    };
  }).catch((error) => {
    ResultStore.setNotWorking();

    return {
      type: 'error',
      error: error
    };
  });
}

function insertLoadingResult(variable) {
  ResultStore.insertResult({
    type: 'Loading',
    expression: `${variable} `,
    result: ''
  });
}
