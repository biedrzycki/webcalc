import DocumentStore from 'stores/DocumentStore';
import DocumentService from 'services/DocumentService';

export default function updateDocument(document, newDocumentParams) {
  DocumentStore.updateDocument(document, newDocumentParams);

  return DocumentService.save(
    document.id,
    newDocumentParams.documentName,
    newDocumentParams.functionName,
    newDocumentParams.functionInputParams,
    newDocumentParams.functionOutputParams,
    newDocumentParams.content
  );
}
