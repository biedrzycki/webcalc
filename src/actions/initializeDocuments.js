import DocumentStore from 'stores/DocumentStore';
import WorkerStore from 'stores/WorkerStore';
import Document from 'models/Document';

export default function initializeDocuments(documents) {
  documents.forEach(document => {
    const documentParams = {
      id: document.id,
      content: document.content,
      documentName: document.title,
      functionName: document.function_name,
      functionInputParams: document.input_parameters,
      functionOutputParams: document.output_parameters
    };

    const newDocument = Document.create(documentParams);
    DocumentStore.addDocument(newDocument);
    WorkerStore.createWorker(newDocument.id);

    DocumentStore.updateFunctions(newDocument.id);
  });
}
