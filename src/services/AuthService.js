import axios from 'axios';
import config from 'config';

class AuthService {
  login(login, password) {
    return axios({
      url: `${config.apiUrl}/users`,
      method: 'post',
      data: {
        login,
        password
      }
    });
  }

  check(token, userId) {
    return axios({
      url: `${config.apiUrl}/users`,
      method: 'post',
      data: {
        token,
        user_id: parseInt(userId, 10)
      }
    });
  }

  logout() {
    return true;
  }
}

export default new AuthService();
