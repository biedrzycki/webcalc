var mathjs = require('mathjs');
var registerPromiseWorker = require('promise-worker/register');
var mathDecorator = require('./MathDecorator');

// config mathjs
mathDecorator.decorate(mathjs);

// create parser
var mathParser = mathjs.parser();

registerPromiseWorker((message) => {
  if (message.type === 'console') {
    return {
      output: mathParser.eval(message.line),
      scope: mathParser.getAll()
    };
  } else if (message.type === 'clear') {
    mathParser.clear();
  } else if (message.type === 'scope') {
    return {
      scope: mathParser.getAll()
    }
  } else if (message.type === 'updateScope') {
    Object.keys(message.scope).forEach(variable => {
      const value =  parseValue(message.scope[variable]);

      console.log(message.scope[variable]);
      console.log(value);

      if (value) {
        mathParser.set(variable.toString(), value);
      }

      console.log(mathParser.getAll());
    });
  }
});

function prepareMatrixData(matrixData) {
  const data = [];

  matrixData.forEach((cell, index) => {
    if (Array.isArray(cell)) {
      data[index] = prepareMatrixData(matrixData[index]);
    } else {
      data[index] = parseValue(matrixData[index]);
    }
  });

  return data;
}

function parseValue(mathjsJSONObject) {
  switch(mathjsJSONObject.mathjs) {
    case 'Complex':
      return mathjs.complex(mathjsJSONObject);
    case 'Decimal':
    case 'BigNumber':
      return mathjs.bignumber(mathjsJSONObject.value);
    case 'SparseMatrix':
    case 'DenseMatrix':
      return mathjs.matrix(prepareMatrixData(mathjsJSONObject.data));
    default:
      return null
  }
}
