import mathjs from 'mathjs';
import PromiseWorker from 'promise-worker';

const MathConsoleWorker = require('worker!./MathConsoleWorker');
const MathWorker = require('worker!./MathWorker');

class ParserService {
  constructor() {
    window.math = mathjs;
    this.mathConsoleWorker = new MathConsoleWorker;
    this.mathWorker = new MathWorker;

    this.PromisifiedMathConsoleWorker = new PromiseWorker(this.mathConsoleWorker);
    this.PromisifiedMathWorker = new PromiseWorker(this.mathWorker);

    this._preservedWords = {};
    this._preservedFunctions = {};

    this.addPreservedWord('pi', () => mathjs.pi);
    this.addPreservedWord('e', () => mathjs.e);
    this.addPreservedWord('i', () => mathjs.i);
    this.addPreservedWord('Infinity', () => mathjs.Infinity);
    this.addPreservedWord('LN2', () => mathjs.LN2);
    this.addPreservedWord('LN10', () => mathjs.LN10);
    this.addPreservedWord('LOG2E', () => mathjs.LOG2E);
    this.addPreservedWord('LOG10E', () => mathjs.LOG10E);
    this.addPreservedWord('NaN', () => mathjs.NaN);
    this.addPreservedWord('null', () => mathjs.null);
    this.addPreservedWord('phi', () => mathjs.phi);
    this.addPreservedWord('SQRT1_2', () => mathjs.SQRT1_2);
    this.addPreservedWord('SQRT2', () => mathjs.SQRT2);
    this.addPreservedWord('tau', () => mathjs.tau);
    this.addPreservedWord('version', () => mathjs.version);
  }

  check(expression) {
    if (typeof this._preservedWords[expression] === 'function') {
      return this._preservedWords[expression]();
    }
  }

  checkFunction(expression) {
    let functionName;

    if (expression.indexOf('=') > -1) {
      functionName = expression.split('=')[1].trim().split('(')[0];
    } else {
      functionName = expression.split('(')[0];
    }

    if (typeof this._preservedFunctions[functionName] === 'function') {
      return this._preservedFunctions[functionName];
    }
  }

  eval(expression) {
    return this.PromisifiedMathConsoleWorker.postMessage({type: 'console', line: expression});
  }

  clear() {
    return this.PromisifiedMathConsoleWorker.postMessage({type: 'clear'});
  }

  get parserScope() {
    return this.PromisifiedMathConsoleWorker.postMessage({type: 'scope'});
  }

  updateScope(scope) {
    return this.PromisifiedMathConsoleWorker.postMessage({type: 'updateScope', scope: scope});
  }

  addPreservedWord(word, callback) {
    this._preservedWords[word] = callback;
  }

  addFunction(name, document) {
    this._preservedFunctions[name] = (...params) => {
      const baseParams = params || [];

      // just a basic check of amount of input parameters, no type checking here...
      if (document.functionInputParamsLength !== params.length) {
        const defaultParams = document.functionInputParams.replace(/\s*/g, '').split(',');

        defaultParams.forEach((defaultParam, index) => {
          if (defaultParam.indexOf('=') > -1 && typeof baseParams[index] === 'undefined') {
            baseParams[index] = defaultParam.split('=')[1];
          }
        });

        if (baseParams.length !== document.functionInputParamsLength) {
          return {
            error: true,
            paramsAmount: document.functionInputParamsLength
          }
        }
      }

      return {
        document: document,
        body: this.PromisifiedMathWorker.postMessage({
          script: document.content,
          type: 'editor',
          inputParams: document.functionInputParams,
          inputParamsValues: baseParams
        })
      }
    };
  }

  removeFunction(name) {
    delete this._preservedFunctions[name];
  }
}

export {ParserService as ParserService}
export default new ParserService();
