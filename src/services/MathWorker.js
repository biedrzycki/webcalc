var mathjs = require('mathjs');
var registerPromiseWorker = require('promise-worker/register');
var mathDecorator = require('./MathDecorator');
var scriptBuilder = require('../helpers/ScriptBuilder');

// config mathjs
mathDecorator.decorate(mathjs);

// create parser
var mathParser = mathjs.parser();

registerPromiseWorker((message) => {
  mathParser.clear();

  if (message.type === 'editor') {
    const script = scriptBuilder.buildScript(message.script, message.inputParams, message.inputParamsValues);
    const before = new Date;

    console.log(script);

    eval(script);

    const after = new Date;

    return {
      scope: mathParser.getAll(),
      time: after - before,
      script: script
    };
  }
});
