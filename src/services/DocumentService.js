import axios from 'axios';
import config from 'config';
import LoginStore from 'stores/LoginStore';

class DocumentService {
  save(id, title, functionName, input, output, content) {
    return axios({
      url: `${config.apiUrl}/documents`,
      method: 'put',
      data: {
        id,
        title,
        function_name: functionName,
        input_parameters: input,
        output_parameters: output,
        content: content,
        token: LoginStore.jwt,
        user_id: LoginStore.user
      }
    });
  }

  remove(id) {
    return axios({
      url: `${config.apiUrl}/documents`,
      method: 'delete',
      data: {
        id,
        token: LoginStore.jwt,
        user_id: LoginStore.user
      }
    });
  }
}

export default new DocumentService();
