import Plot from '../models/Plot';
import gauss from 'helpers/scripts/gauss';
import transform, {inputReals, zeroReals} from 'helpers/scripts/fft';
import parserConfig from './parserConfig';

module.exports.decorate = function (mathObject) {
  mathObject.config(parserConfig);

  addFunction(mathObject, 'plot', (...data) => {
    return new Plot(data);
  });

  addFunction(mathObject, 'gaussRandom', (...data) => {
    return gauss(...data);
  });

  addFunction(mathObject, 'randomMatrix', (...data) => {
    let randomMatrix = [];

    if (data.length === 2) {
      for (let i = 0; i < data[0]; i++) {
        randomMatrix[i] = [];

        for (let j = 0; j < data[1]; j++) {
          randomMatrix[i][j] = mathObject.bignumber(Math.random());
        }
      }
    } else {
      for (let i = 0; i < data[0]; i++) {
        randomMatrix[i] = mathObject.bignumber(Math.random());
      }
    }

    return mathObject.matrix(randomMatrix);
  });

  addFunction(mathObject, 'fft', (...data) => {
    const iterations = data[2] || 1;
    let realPart;
    let imPart;

    if (data[0] && data[1]) {
      realPart = data[0] ? data[0].toArray() : null;
      imPart = data[1] ? data[1].toArray() : null;
    }

    let total = 0.0;
    let size = 0;

    console.time('fft');

    if (realPart && imPart) {
      size = realPart.length;
    } else {
      size = data[0].toNumber();
    }

    let real;
    let imag;

    for (let i = 0; i < 2*iterations; ++i) {

      if (realPart && imPart) {
        real = new Float32Array(realPart);
        imag = new Float32Array(imPart);
      } else {
        real = inputReals(size);
        imag = zeroReals(size);
      }

      transform(real, imag);

      for (let j = 0; j < size; ++j) {
        total += Math.sqrt(real[j] * real[j] + imag[j] * imag[j]);
      }
    }

    console.timeEnd('fft');

    let finalFFT = [];

    for (let i = 0; i < size; i++) {
      finalFFT[i] = mathObject.complex(real[i], imag[i]);
    }

    return mathObject.matrix(finalFFT);
  });

  mathObject.import(require('numbers'), {wrap: true, silent: true});
};

function addFunction(math, functionName, functionCallback, options = {}) {
  math.import({
    [functionName]: functionCallback
  }, options);
}
