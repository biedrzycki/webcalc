import axios from 'axios';
import config from 'config';
import LoginStore from 'stores/LoginStore';

class FileService {
  load() {
    return axios({
      url: `${config.apiUrl}/files`,
      method: 'put',
      data: {
        token: LoginStore.jwt,
        user_id: LoginStore.user
      }
    });
  }

  add(id, content) {
    return axios({
      url: `${config.apiUrl}/files`,
      method: 'post',
      data: {
        id,
        content: content,
        token: LoginStore.jwt,
        user_id: LoginStore.user
      }
    });
  }

  remove(id) {
    return axios({
      url: `${config.apiUrl}/files`,
      method: 'delete',
      data: {
        id,
        token: LoginStore.jwt,
        user_id: LoginStore.user
      }
    });
  }
}

export default new FileService();
